class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    @board = ''
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    length.times { @board << '_' }
  end

  def take_turn(guess = @guesser.guess)
    response = @referee.check_guess(guess)
    @guesser.handle_response(guess, response)
    update_board(guess, response)
  end

  def update_board(guess, response)
    response.each { |x| @board[x] = guess }
  end

end

class HumanPlayer

  def initialize(name)
    @name = name
    @guesses = 7
  end

  def update_board(board)
    @board = board
  end

  def pick_secret_word
    puts 'Think of a secret word.'
    puts 'How many letters are in your word? (e.g. 5)'
    register_secret_length(gets.chomp.to_i)
  end

  def register_secret_length(length)
    @length = length
  end

  def check_guess(a)
    puts "Guesser guessed a guess: #{a}"
    puts 'Enter the indices of the blanks that contain the letter (e.g. 1,2)'
    gets.chomp.split(',').map(&:to_i)
  end

  def handle_response(guess, response)
    @guesses -= 1 if response.size.zero?
    response.each { |x| @board[x] = guess }
    puts "Guesses remaining: #{@guesses}"
    print @board
  end

  def guess(board)
    puts 'Enter a letter (e.g. a)'
    gets.chomp.downcase
  end
end

class ComputerPlayer

  def initialize(dict = File.readlines('lib/dictionary.txt'))
    @dictionary = dict
  end

  def update_board(board)
    @board = board
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.size
  end

  def register_secret_length(length)
    @candidates = @dictionary.select { |x| x.size == length }
    @length = length
  end

  def check_guess(a)
    raise unless ('a'..'z').to_a.include? a
    indices = []
    @word.each_char.with_index { |c, i| indices << i if c == a }
    indices
  end

  def candidate_words
    @candidates
  end

  def handle_response(guess, response)
    @candidates.select! do |x|
      response.all? { |i| x[i] == guess && x.count(guess) == response.size }
    end
    @candidates.select! { |x| !x.include? guess } if response.size.zero?
  end

  def guess(board)
    letters = Hash.new(0)
    @candidates.each { |x| x.each_char { |c| letters[c] += 1 } }
    letters.delete_if { |k, _| board.include? k }
    letters.max_by { |_, v| v }[0]
  end


end
